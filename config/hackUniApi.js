export default {
  setStorageSync: localStorage.setItem,
  getStorageSync: localStorage.getItem,
};
