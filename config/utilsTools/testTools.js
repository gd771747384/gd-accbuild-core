const testEmail = (val) => {
  return /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(val);
};

const testMobile = (val) => {
  return /^[1][3,4,5,7,8][0-9]{9}$/.test(val);
};

export default {
  testEmail,
  testMobile,
};
