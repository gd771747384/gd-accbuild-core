export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Adv-ButtonEmitCodeEditor",
          default: "{}",
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "max",
          label: "max",
          type: "Input",
          componentAttr: {
            type: "number",
          },
          default: 5,
        },
        docLinkConfig: {},
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "allow-half",
          label: "allow-half",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "low-threshold",
          label: "low-threshold",
          type: "Input",
          componentAttr: {
            type: "number",
          },
          default: 2,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "high-threshold",
          label: "high-threshold",
          type: "Input",
          componentAttr: {
            type: "number",
          },
          default: 4,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "colors",
          label: "colors",
          type: "Adv-ButtonEmitCodeEditor",
          componentAttr: {
            lang: "json",
          },
          default: ["#F7BA2A", "#F7BA2A", "#F7BA2A"],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "void-icon",
          label: "void-icon",
          type: "Input",
          default: "Star",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled-void-icon",
          label: "disabled-void-icon",
          type: "Input",
          default: "StarFilled",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "show-text",
          label: "show-text",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "show-score",
          label: "show-score",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "text-color",
          label: "text-color",
          type: "Input",
          default: "#1F2D3D",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "texts",
          label: "texts",
          type: "Adv-ButtonEmitCodeEditor",
          componentAttr: {
            lang: "json",
          },
          default: ["Extremely bad", "Disappointed", "Fair", "Satisfied", "Surprise"],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "clearable",
          label: "clearable",
          type: "Switch",
          default: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
