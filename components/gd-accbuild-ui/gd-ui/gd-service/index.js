import loadingService from "./gd-loading-service/gd-loading-service.js"
import toastService from "./gd-toast-service/gd-toast-service.js"
import {main} from "./gd-container-service/gd-container-service.js"
export default{
    container:main,
    loading:loadingService,
    toast:toastService,
}