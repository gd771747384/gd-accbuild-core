export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Adv-ButtonEmitCodeEditor",
          default: "{}",
          componentAttr: {
            lang: "json",
          },
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "icon",
          label: "icon",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "shape",
          label: "shape",
          type: "Select",
          default: "circle",
          options: [
            {
              label: "circle",
              value: "circle",
            },
            {
              label: "square",
              value: "square",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "src",
          label: "src",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "src-set",
          label: "src-set",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "alt",
          label: "alt",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "fit",
          label: "fit",
          type: "Select",
          default: "cover",
          options: [
            {
              label: "fill",
              value: "fill",
            },
            {
              label: "contain",
              value: "contain",
            },
            {
              label: "cover",
              value: "cover",
            },
            {
              label: "none",
              value: "none",
            },
            {
              label: "scale-down",
              value: "scale-down",
            },
          ],
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
