export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "type",
          label: "type",
          type: "Select",
          default: "info",
          options: [
            {
              label: "success",
              value: "success",
            },
            {
              label: "info",
              value: "info",
            },
            {
              label: "warning",
              value: "warning",
            },
            {
              label: "danger",
              value: "danger",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "closable",
          label: "closable",
          type: "Switch",
          default: "false",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disable-transitions",
          label: "disable-transitions",
          type: "Switch",
          default: "false",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "hit",
          label: "hit",
          type: "Switch",
          default: "false",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "color",
          label: "color",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "effect",
          label: "effect",
          type: "Select",
          default: "light",
          options: [
            {
              label: "dark",
              value: "dark",
            },
            {
              label: "light",
              value: "light",
            },
            {
              label: "plain",
              value: "plain",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "round",
          label: "round",
          type: "Switch",
          default: "false",
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "click",
          label: "click",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "close",
          label: "close",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
