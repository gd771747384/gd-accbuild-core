export default {
  mobile: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         */
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "placeholder",
          label: "placeholder",
          type: "Input",
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "default",
          label: "默认值",
          type: "Input",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "readonly",
          label: "readonly",
          type: "Switch",
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          type: "Switch",
          default: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
        },
      },
    ],
  },
  pc: {
    attrs: [
      {
        /**
         * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
         * 当pathInJson为""时,属性位于metaItem.里面
         * 当pathInJson为"propOptions"时,属性位于propOptions  里面
         */
        pathInJson: "propOptions",
        uiConfigInPanel: {
          key: "isDefaultShow",
          label: "是否默认显示",
          type: "Switch",
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "style",
          label: "容器style",
          type: "Input",
        },
      },
      {
        pathInJson: "",
        uiConfigInPanel: {
          key: "default",
          label: "默认值",
          type: "Select",
          options: [
            {
              label: "true",
              value: true,
            },
            {
              label: "false",
              value: false,
            },
          ],
          default: false,
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "size",
          label: "尺寸",
          type: "Select",
          default: "default",
          options: [
            {
              label: "large",
              value: "large",
            },
            {
              label: "default",
              value: "default",
            },
            {
              label: "small",
              value: "small",
            },
          ],
        },
      },
      {
        pathInJson: undefined,
        uiConfigInPanel: {
          key: "disabled",
          label: "disabled",
          _isIndepenceInAddOrEdit: true, //默认新增编辑的表单相同;当此标记为true时,即新增编辑弹窗的表单不相同,
          type: "Switch",
          default: false,
        },
      },
    ],
    rules: [],
    events: [
      {
        pathInJson: "events",
        uiConfigInPanel: {
          key: "change",
          label: "change",
          type: "Adv-ButtonEmitCodeEditor",
          default: "",
          componentAttr: {
            textWrapperConfig: {
              prefixText: "function(configObj) {\n",
              suffixText: "}",
            },
          },
        },
      },
    ],
  },
};
