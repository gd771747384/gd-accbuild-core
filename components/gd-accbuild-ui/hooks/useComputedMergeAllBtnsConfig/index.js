import { computed, ref } from "vue";
/**
 * @param {Object} btnsConfigRefProxy 用户自定义的按钮配置(可以覆盖下面的配置)
 * @param {Object} buildInBtnsDefaultConfig 内建的按钮配置
 * @param {Function} emits
 * @param {Boolean} isExitLoop 是否退出递归
 */
const getMergeBtnsConfig = ({
  btnsConfig,
  buildInBtnsDefaultConfig,
  emits,
  isExitLoop = false,
  isTreeTable = false,
}) => {
  if (isExitLoop) {
    return [];
  }
  //console.log(btnsConfig, buildInBtnsDefaultConfig, "按钮配置====");
  const buttons = btnsConfig?.buttons ?? [];
  const buildInBtns = Object.keys(buildInBtnsDefaultConfig);
  const buildInBtnsConfig = buildInBtns.map((btnKey) => {
    let btnObj = buttons.find((btn) => btn.key === btnKey);
    /* if (btnsConfig.containerStyle === "Tip") {
            btnObj = buildInBtnsDefaultConfig[btnKey]
        } */
    if (btnObj) {
      console.log(btnObj,buildInBtnsDefaultConfig,'按钮配置输出')
      const buildInBtnsDefaultContainerBtnsConfig = {};
      const arr =
        buildInBtnsDefaultConfig?.[btnKey]?.containerAttr?.buttons ?? [];
      arr.forEach((el) => {
        buildInBtnsDefaultContainerBtnsConfig[el.key] = el;
      });
      /////////////////////////////
      const getLabel = () => {
        if (isTreeTable && btnKey === "buildInAddBtn") {
          return "添加一级节点";
        } else {
          return btnObj?.label ?? buildInBtnsDefaultConfig[btnKey].label;
        }
      };
      const getIsShow = () => {
        if (isTreeTable && btnKey === "buildInAddChildIntoCurBtn") {
          return (
            btnObj?.isShow ?? buildInBtnsDefaultConfig[btnKey]?.isShow ?? true
          );
        } else {
          return btnObj?.isShow ?? buildInBtnsDefaultConfig[btnKey].isShow;
        }
      };
      //console.log(btnObj,'按钮对象')
      ////////////////////////////
      return {
        key: btnKey,
        label: getLabel(),
        isShow: getIsShow(),
        isShowFunc: btnObj?.isShowFunc ?? null,
        beforeClickFunc:
          btnObj?.beforeClickFunc ??
          buildInBtnsDefaultConfig[btnKey].beforeClickFunc,
        onClick: btnObj?.onClick ?? buildInBtnsDefaultConfig[btnKey].onClick,
        componentAttr: {
          type:
            btnObj?.componentAttr?.type ??
            buildInBtnsDefaultConfig[btnKey].componentAttr.type,
          icon:
            btnObj?.componentAttr?.icon ??
            buildInBtnsDefaultConfig[btnKey].componentAttr.icon,
          size: btnsConfig?.size ?? "default",
        },
        containerAttr: {
          title:
            btnObj?.containerAttr?.title ??
            buildInBtnsDefaultConfig[btnKey].containerAttr.title,
          containerStyle:
            btnObj?.containerAttr?.containerStyle ??
            buildInBtnsDefaultConfig[btnKey].containerAttr.containerStyle,
          //优化下面代码
          buttons:
            arr.length > 0
              ? arr
              : getMergeBtnsConfig({
                  btnsConfig: btnObj?.containerAttr ?? {},
                  buildInBtnsDefaultConfig:
                    buildInBtnsDefaultContainerBtnsConfig,
                  isExitLoop: true,
                  isTreeTable,
                }),
        },
        vmCode: btnObj?.vmCode,
      };
    } else {
      return buildInBtnsDefaultConfig[btnKey];
    }
  });
  const otherBtns = buttons.filter((btn) => !buildInBtns.includes(btn.key));
  const otherBtnsConfig = otherBtns.map((el, idx) => {
    return {
      key: el.key,
      label: el.label,
      isShow: el?.isShow ?? true,
      onClick:
        el?.onClick ??
        ((args) => {
          emits("do-operation", args);
        }),
      componentAttr: {
        type: el?.componentAttr?.type ?? "primary",
        icon: el?.componentAttr?.icon ?? "",
        size: btnsConfig?.size ?? el?.componentAttr?.size ?? "default",
      },
    };
  });
  return [...buildInBtnsConfig, ...otherBtnsConfig];
};

/**
 * 获取内建按钮和自定义按钮的配置(toobar按钮、操作按钮)
 * @param {Object} btnsConfigRefProxy 用户自定义的按钮配置(可以覆盖下面的配置)
 * @param {Object} configObj 其他配置信息
 * @param {Object} configObj.buildInBtnsDefaultConfig 内建的按钮配置
 * @param {Function} configObj.emits
 * @param {Boolean} configObj.isExitLoop 是否退出递归
 *  */
export const useComputedMergeAllBtnsConfig = (
  btnsConfigRefProxy,
  { buildInBtnsDefaultConfig, emits, isExitLoop = false },
  isTreeTableRefProxy = { value: false }
) => {
  if (isExitLoop) {
    return ref([]);
  }
  const buildInBtns = Object.keys(buildInBtnsDefaultConfig);
  /////初始化默认容器配置
  buildInBtns.forEach((key) => {
    if (buildInBtnsDefaultConfig[key].hasOwnProperty("containerAttr")) {
      if (
        !buildInBtnsDefaultConfig[key].containerAttr.hasOwnProperty("title")
      ) {
        buildInBtnsDefaultConfig[key].containerAttr.title =
          buildInBtnsDefaultConfig[key].label;
      }
      if (
        !buildInBtnsDefaultConfig[key].containerAttr.hasOwnProperty(
          "containerStyle"
        )
      ) {
        buildInBtnsDefaultConfig[key].containerAttr.containerStyle = "Dialog";
      }
    } else {
      buildInBtnsDefaultConfig[key].containerAttr = {
        title: buildInBtnsDefaultConfig[key]?.label ?? "",
        containerStyle: "Dialog",
      };
    }
  });
  //console.log(buildInBtnsDefaultConfig,'内建哈哈哈哈')
  return computed(() => {
    return getMergeBtnsConfig({
      btnsConfig: btnsConfigRefProxy.value,
      buildInBtnsDefaultConfig,
      emits,
      isExitLoop: false,
      isTreeTable: isTreeTableRefProxy.value,
    });
  });
};
