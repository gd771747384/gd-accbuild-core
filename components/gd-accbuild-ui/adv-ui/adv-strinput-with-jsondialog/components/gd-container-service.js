import { ref, computed } from 'vue'


const containerProps = ref({
    isShowContainer: false, containerType: "Dialog", containerAttr: {}, onDoOperation: ({ eventFrom, ...params }) => { }
})
export default options => {
    if (typeof options === "object" && Object.keys(options).length > 0) {
        /* Object.keys(options).forEach(key=>{
            containerProps.value[key] = options[key]
        }) */
        containerProps.value = options
    }
    return containerProps
}

export const setContainerProps = (options) => {
    if (typeof options === "object" && Object.keys(options).length > 0) {
        Object.keys(options).forEach(key => {
            containerProps.value[key] = options[key]
        })
    }
}

export const computedContainerProps = computed(() => containerProps.value)

export const toggleShowContainer = () => {
    containerProps.value.isShowContainer = !containerProps.value.isShowContainer
}