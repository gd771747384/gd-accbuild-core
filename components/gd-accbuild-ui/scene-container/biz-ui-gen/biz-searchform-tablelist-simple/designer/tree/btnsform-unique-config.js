export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "type",
                label: "显示类型",
                type: "Select",
                default: "text",
                options: [{
                    label: "text",
                    value: "text"
                }, {
                    label: "textarea",
                    value: "textarea"
                }, {
                    label: "password",
                    value: "password"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "placeholder",
                label: "placeholder",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "size",
                label: "尺寸",
                type: "Select",
                default: "default",
                options: [{
                    label: "large",
                    value: "large"
                }, {
                    label: "default",
                    value: "default"
                }, {
                    label: "small",
                    value: "small"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "autosize",
                label: "autosize",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "maxlength",
                label: "maxlength",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "readonly",
                label: "readonly",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "disabled",
                label: "disabled",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showWordLimit",
                label: "统计字数提示",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "clearable",
                label: "清除按钮",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showPassword",
                label: "密码显隐按钮",
                type: "Switch",
                default: false
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "blur",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "focus",
                label: "focus",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "input",
                label: "input",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "clear",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    },
    "pc": {
        attrs: [{
            /**
             * 在Resource的uiOptions里面
             */
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr.treeTableConfig",
            uiConfigInPanel: {
                key: "key",
                label: "按钮Key",
                type: "Input",
                default: ""
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "label",
                label: "按钮文字",
                type: "Input",
                default: "",
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "containerStyle",
                label: "触发的容器风格",
                type: "Select",
                options: [{
                    label: "提示框",
                    value: "Tip"
                }, {
                    label: "弹窗",
                    value: "Dialog"
                }, {
                    label: "抽屉",
                    value: "Drawer"
                }, {
                    label: "新开标签页",
                    value: "_tab"
                }, {
                    label: "新开页面",
                    value: "_blank"
                }],
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "operationType",
                label: "按钮执行操作",
                type: "Select",
                options: [{
                    label: "新增",
                    value: "Add"
                }, {
                    label: "批量删除",
                    value: "BatchDelete"
                }, {
                    label: "编辑",
                    value: "Edit"
                }, {
                    label: "查看详情",
                    value: "Get"
                }],
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "dataFromType",
                label: "按钮接口来自",
                type: "Select",
                options: [{
                    label: "VmCode",
                    value: "VmCode"
                }, {
                    label: "Api",
                    value: "Api"
                }],
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "vmCode",
                label: "视图模型",
                type: "Select",
                options: [{
                    label: "当前视图模型",
                    value: "当前视图模型"
                }],
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "beforeClickFunc",
                label: "按钮点击前拦截",
                type: "Adv-ButtonEmitCodeEditor"
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "submitFunc",
                label: "提交表单事件",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["btnsform"]
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "onFormValidateFunc",
                label: "表单触发校验",
                type: "Adv-ButtonEmitCodeEditor",
            },
            belongToTemplateCompNames: ["btnsform"]
        },/* , {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "visible-change",
                label: "提交表单事件",
                type: "Adv-ButtonEmitCodeEditor",
            }
        } */]
    }
}