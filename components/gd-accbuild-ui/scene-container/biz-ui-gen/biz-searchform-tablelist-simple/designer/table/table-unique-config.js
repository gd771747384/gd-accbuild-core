export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "type",
                label: "显示类型",
                type: "Select",
                default: "text",
                options: [{
                    label: "text",
                    value: "text"
                }, {
                    label: "textarea",
                    value: "textarea"
                }, {
                    label: "password",
                    value: "password"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "placeholder",
                label: "placeholder",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "size",
                label: "尺寸",
                type: "Select",
                default: "default",
                options: [{
                    label: "large",
                    value: "large"
                }, {
                    label: "default",
                    value: "default"
                }, {
                    label: "small",
                    value: "small"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "autosize",
                label: "autosize",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "maxlength",
                label: "maxlength",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "readonly",
                label: "readonly",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "disabled",
                label: "disabled",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showWordLimit",
                label: "统计字数提示",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "clearable",
                label: "清除按钮",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showPassword",
                label: "密码显隐按钮",
                type: "Switch",
                default: false
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "blur",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "focus",
                label: "focus",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "input",
                label: "input",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "clear",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    },
    "pc": {
        /* buildInColumnConfig: {
            buildInSeq: {
              isShow: false,
            },
            buildInCheckbox: {
              isShow: true,
              config: {
                //checkAll: true,
                checkRowKeys: [],
                //labelField: "propCode",
              },
            },
            buildInExpand: {
              isShow: false,
              expandContentType: "Table",
              isInheritMomTableCommonAttr: true,
              config: {
                labelField: "propCode",
                lazy: true,
                loadMethod: getLoadChildrenMethod(),
                visibleMethod: ({ column, columnIndex, row, rowIndex }) =>
                  ["OneToOne", "OneToMany", "ManyToOne", "ManyToMany"].includes(
                    row["_gd_relationType"]
                  ),
              },
            },
          }, */
        attrs: [{
            /**
             * 在Resource的uiOptions里面
             */
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr.buildInColumnConfig.buildInSeq",
            uiConfigInPanel: {
                key: "isShow",
                label: "内置列-序号-是否显示",
                type: "Switch",
                default: true
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr.buildInColumnConfig.buildInCheckbox",
            uiConfigInPanel: {
                key: "isShow",
                label: "内置列-多选-是否显示",
                type: "Switch",
                default: true
            }
        }, {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "isPaged",
                label: "是否分页",
                type: "Switch",
                default: "true",
            }
        }/* , {
            pathInJson: "pageUiTemplateConfigs.$.templateProps.tableCommonAttr",
            uiConfigInPanel: {
                key: "isShowBuildInMulti",
                label: "表格行是否多选",
                type: "Switch",
                default: "true",
            }
        } */],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "表格单元格点击",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "visible-change",
                label: "表格行选中",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "remove-tag",
                label: "表格多选改变",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "表格数据改变",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "表格单元格双击",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    }
}