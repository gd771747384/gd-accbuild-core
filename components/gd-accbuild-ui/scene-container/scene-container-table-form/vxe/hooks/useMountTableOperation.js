import { onMounted } from "vue"
export default (tableOperationMethods, vxeTableRef, props,flattenTableBodyData) => {
    onMounted(() => {
        tableOperationMethods.insertAt = (records, row = -1) => {
            //console.log(cloneDeep(records));
            vxeTableRef.value.insertAt(records, row);
            let operator = "push";
            if (row === null) {
                operator = "unshift";
            } else {
                operator = "push";
            }
            if (Array.isArray(records)) {
                props.tableBodyData[operator](...records);
            } else {
                props.tableBodyData[operator](records);
            }
        };
        tableOperationMethods.remove = (rows) => {
            vxeTableRef.value.remove(rows);
            if (Array.isArray(rows)) {
                rows.forEach((row) => {
                    const matchRowIdx = flattenTableBodyData.value.findIndex(
                        (el) => el.id === row.id
                    );
                    flattenTableBodyData.value.splice(matchRowIdx, 1);
                });
            } else {
                const matchRowIdx = flattenTableBodyData.value.findIndex(
                    (el) => el.id === rows.id
                );
                flattenTableBodyData.value.splice(matchRowIdx, 1);
            }
        };
        tableOperationMethods.updateRowsData = (records) => {
            flattenTableBodyData.value.forEach((el, idx) => {
                const matchRow = records.find((row) => row.id === el.id);
                if (matchRow) {
                    Object.keys(el).forEach(key => {
                        el[key] = matchRow[key]
                    })
                }
            });
        };
        tableOperationMethods.setActiveRow = (row) => {
            vxeTableRef.value.setActiveRow(row);
        };
        tableOperationMethods.setEditRow = (row) => {
            vxeTableRef.value.setEditRow(row);
        };
        tableOperationMethods.clearEdit = () => vxeTableRef.value.clearEdit();
        tableOperationMethods.getTableData = () => vxeTableRef.value.getTableData();
        tableOperationMethods.refreshTableMetaData = () => {
            vxeTableRef.value.refreshColumn()
        };
    });
}