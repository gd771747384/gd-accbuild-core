import { computed } from "vue"
import globalStyles from "@gd-accbuild-core/theme-chalk/pc-admin/js.module.scss"

/**
 * 计算表格的动态宽高尺寸
 */
export default (props) => {
    const isShowToolBar = computed(() => {
        return (
            Array.isArray(props.toolBarConfig?.buttons) &&
            props.toolBarConfig.buttons.length > 0 &&
            props.toolBarConfig.buttons.some((btn) => btn?.isShow ?? true)
        );
    });
    const isShowSearchForm = computed(() => {
        return (
            props.searchFormItemMetaList.length > 0 &&
            props.searchFormItemMetaList.some((meta) => meta?.isShow ?? true)
        );
    });
    const isInlineEditable = computed(() => {
        return props.tableCommonAttr?.editConfig?.editableStyle === "Inline";
    });
    const isShowBuildInAddRow = computed(() => {
        return (
            props.tableCommonAttr?.editConfig?.buildInAddRowConfig?.isShow ??
            isInlineEditable.value
        );
    });
    const isShowToolBarContainer = computed(() => {
        return (
            !isInlineEditable.value && (isShowToolBar.value || isShowSearchForm.value)
        );
    });
    // 表格高度动态响应 开始
    const cutPageTopWindowContainerHeight = globalStyles.accbuildPageTopWindowContainerHeight; //#{$--accbuild-page-top-window-container-height}
    const cutPageTagsContainerHeight = globalStyles.accbuildPageTagsContainerHeight; //#{$--accbuild-page-tags-container-height}
    const cutDefaultContainerGutter = globalStyles.accbuildDefaultContainerGutter; //#{$--accbuild-default-container-gutter}
    const computedToolBarContainerHeight = computed(() => {
        let cutToolBarContainerHeight = "0px";
        if (isShowToolBarContainer.value) {
            cutToolBarContainerHeight = `(${cutPageTagsContainerHeight} + 2 * ${cutDefaultContainerGutter})`;
        }
        return cutToolBarContainerHeight;
    });
    const computedPaginationHeight = computed(() => {
        let cutPaginationHeight = "0px";
        if (props.tableCommonAttr?.isPaged ?? false) {
            cutPaginationHeight = `(${globalStyles.accbuildTablePaginationHeightDefault} + 2 * ${cutDefaultContainerGutter})`
        }
        return cutPaginationHeight;
    });
    const computedTableContentContainerHeight = computed(() => {
        const toolBarContainerHeight = computedToolBarContainerHeight.value;
        const paginationHeight = computedPaginationHeight.value;
        return `calc(100% - ${toolBarContainerHeight} - ${paginationHeight})`
        return (
            props.tableCommonAttr?.tableContentContainerConfig?.height ??
            `calc(100vh - ${cutPageTopWindowContainerHeight} - ${cutPageTagsContainerHeight} - 2 * ${cutDefaultContainerGutter} - ${computedToolBarContainerHeight.value} - ${computedPaginationHeight.value})`
        );
    });
    const computedTableFormContainerHeight = computed(() => {
        const tableContentContainerHeight = computedTableContentContainerHeight.value;
        const toolBarContainerHeight = computedToolBarContainerHeight.value;
        const paginationHeight = computedPaginationHeight.value;
        const isWrapWithCalc = tableContentContainerHeight.startsWith("calc(");
        if (isWrapWithCalc) {
            return `${tableContentContainerHeight.slice(
                0,
                -1
            )} + ${toolBarContainerHeight} + ${paginationHeight})`;
        } else {
            return `calc(${tableContentContainerHeight} + ${toolBarContainerHeight} + ${paginationHeight})`;
        }
    });
    const computedTableWidth = computed(() => {
        return `calc(100% - 2 * ${cutDefaultContainerGutter})`;
    });
    // 表格高度动态响应 结束
    return {
        isShowToolBar,
        isShowSearchForm,
        isInlineEditable,
        isShowBuildInAddRow,
        isShowToolBarContainer,
        ///////////
        computedTableContentContainerHeight,
        computedTableFormContainerHeight,
        cutPageTagsContainerHeight,
        cutDefaultContainerGutter,
        //////////
        computedTableWidth
    }
}