import { inject, defineComponent, provide, h } from "vue"
import CustomizeTreeNode from "./customize-tree-node/index.vue";
/**
 * 树自定义节点渲染
 */
export default (myEmits, operationBtnsConfig, treeNodeConfig) => {
    // #ifdef H5
    const OverrideCustomizeTreeNode = inject("CustomizeTreeNode", null);
    const DefaultCustomizeTreeNode = defineComponent({
        provide: {
            myEmits: myEmits,
            operationBtnsConfig,
            treeNodeConfig: treeNodeConfig,
        },
        setup(props, { slots, attrs, emit }) {
            return () => {
                if (OverrideCustomizeTreeNode) {
                    return h(OverrideCustomizeTreeNode, () => [{ attrs: attrs }]);
                } else {
                    return h(CustomizeTreeNode, () => [{ attrs: { ...attrs } }]);
                }
            };
        },
    });
    provide("CustomizeTreeNode", DefaultCustomizeTreeNode);
    provide("isCustomizeTreeNode", true);
    // #endif
}