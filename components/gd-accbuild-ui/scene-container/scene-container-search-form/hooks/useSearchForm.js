import {toRef} from "vue"
import { resetFormData } from "@gd-accbuild-ui/gd-ui/utils"
export default (props) => {
    const onReset = () => {
        resetFormData(props.formData, props.formItemMetaList)
    }
    return {
        onReset 
    }
}