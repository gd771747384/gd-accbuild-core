import { inject, useAttrs } from "vue";
// #ifdef H5
import { defineComponent, h, computed } from "vue";

export const DefaultCustomizeTreeNode = defineComponent({
  setup(props, { slots, attrs, emit }) {
    console.log(attrs.node.label, "============");
    const label = computed(() => attrs.node.label);
    return {
      _attrs: attrs,
      label,
    };
  },
  render() {
    return <view>{this.label}</view>;
  },
});
export const CustomizeTreeNode = DefaultCustomizeTreeNode; //inject("CustomizeTreeNode", DefaultCustomizeTreeNode);
// #endif
export const isCustomizeTreeNode = inject("isCustomizeTreeNode", false);
