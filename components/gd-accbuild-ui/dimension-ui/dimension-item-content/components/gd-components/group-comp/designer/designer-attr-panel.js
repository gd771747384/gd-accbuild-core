export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "type",
                label: "显示类型",
                type: "Select",
                default: "text",
                options: [{
                    label: "text",
                    value: "text"
                }, {
                    label: "textarea",
                    value: "textarea"
                }, {
                    label: "password",
                    value: "password"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "placeholder",
                label: "placeholder",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "size",
                label: "尺寸",
                type: "Select",
                default: "default",
                options: [{
                    label: "large",
                    value: "large"
                }, {
                    label: "default",
                    value: "default"
                }, {
                    label: "small",
                    value: "small"
                }]
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "autosize",
                label: "autosize",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "maxlength",
                label: "maxlength",
                type: "Input",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "readonly",
                label: "readonly",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "disabled",
                label: "disabled",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showWordLimit",
                label: "统计字数提示",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "clearable",
                label: "清除按钮",
                type: "Switch",
                default: false
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "showPassword",
                label: "密码显隐按钮",
                type: "Switch",
                default: false
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "blur",
                label: "blur",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "focus",
                label: "focus",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "input",
                label: "input",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }, {
            pathInJson: "events",
            uiConfigInPanel: {
                key: "clear",
                label: "clear",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    },
    "pc": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             * 当pathInJson为"propOptions"时,属性位于propOptions  里面
             */
            pathInJson: "propOptions",
            uiConfigInPanel: {
                key: "isDefaultShow",
                label: "是否默认显示",
                type: "Switch",
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "groupInfo",
                label: "组合方式",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }],
        rules: [],
        events: [{
            pathInJson: "events",
            uiConfigInPanel: {
                key: "change",
                label: "change",
                type: "Adv-ButtonEmitCodeEditor",
            }
        }]
    }
}