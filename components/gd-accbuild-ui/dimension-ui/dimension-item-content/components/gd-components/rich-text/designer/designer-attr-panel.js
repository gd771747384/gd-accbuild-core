
export default {
    "mobile": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             */
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Input",
            }
        }],
        rules: [],
        events: []
    },
    "pc": {
        attrs: [{
            /**
             * 当pathInJson为undefined/"ComponentAttr"时,属性位于metaItem.componentAttr.里面
             * 当pathInJson为""时,属性位于metaItem.里面
             * 当pathInJson为"propOptions"时,属性位于propOptions  里面
             */
            pathInJson: "propOptions",
            uiConfigInPanel: {
                key: "isDefaultShow",
                label: "是否默认显示",
                type: "Switch",
                default: true
            }
        }, {
            pathInJson: undefined,
            uiConfigInPanel: {
                key: "style",
                label: "容器style",
                type: "Adv-ButtonEmitCodeEditor",
                default: "{}",
                componentAttr: {
                    lang: "json"
                }
            }
        }, {
            pathInJson: "",
            uiConfigInPanel: {
                key: "default",
                label: "默认值",
                type: "Input",
                default: ""
            }
        }],
        rules: [],
        events: []
    }
}