/* eslint-disable */

/**
 * !--------- FBI WARNING ----------!
 *
 * 根据 /packages 目录下的组件所生成的模块导出，请勿手动修改
 */
const targetPlatform = "pc-admin";
//import { App, Plugin } from 'vue';

//import 'xe-utils'
//import 'vxe-table/lib/style.css'
//import VXETable from 'vxe-table'
import {
  // 核心
  VXETable,

  // 表格功能
  Footer,
  Icon as VxeIcon,
  Filter,
  Edit,
  Menu,
  Export,
  Keyboard,
  Validator,

  // 可选组件
  Column,
  Colgroup,
  Grid,
  //Tooltip,
  Toolbar,
  Pager,
  // Form,
  // FormItem,
  // FormGather,
  // Checkbox,
  // CheckboxGroup,
  // Radio,
  // RadioGroup,
  // RadioButton,
  // Switch,
  //Input,
  //Select,
  // Optgroup,
  //Option,
  // Textarea,
  //Button,
  // Modal,
  // List,
  // Pulldown,

  // 表格
  Table,
} from "vxe-table";
//import { createI18n } from 'vue-i18n'
import zhCN from "vxe-table/lib/locale/lang/zh-CN";
import enUS from "vxe-table/lib/locale/lang/en-US";

import VXETablePluginElement from "vxe-table-plugin-element";
//import 'vxe-table-plugin-element/dist/style.css'
const useVxeTable = (app) => {
  // 表格功能
  //app.use(VxeHeader)
  app
    .use(Footer)
    .use(VxeIcon)
    .use(Filter)
    .use(Edit)
    .use(Menu)
    .use(Export)
    .use(Keyboard)
    .use(Validator)

    // 可选组件
    .use(Column)
    .use(Colgroup)
    .use(Grid)
    // .use(Tooltip)
    .use(Toolbar)
    .use(Pager)
    // .use(Form)
    // .use(FormItem)
    // .use(FormGather)
    // .use(Checkbox)
    // .use(CheckboxGroup)
    // .use(Radio)
    // .use(RadioGroup)
    // .use(RadioButton)
    // .use(Switch)
    //.use(Input)
    //.use(Select)
    // .use(Optgroup)
    //.use(Option)
    // .use(Textarea)
    //.use(Button)
    // .use(Modal)
    // .use(List)
    // .use(Pulldown)

    // 安装表格
    .use(Table);
  VXETable.use(VXETablePluginElement);
};
const messages = {
  zh_CN: {
    ...zhCN,
  },
  en_US: {
    ...enUS,
  },
};

/* const i18n = createI18n({
    locale: 'zh_CN',
    messages,
}) */

export const AccBuildPlugin = {
  install(app) {
    /* VXETable.setup({
            // 对组件内置的提示语进行国际化翻译
            i18n: (key, args) => i18n.global.t(key, args)
        }) */
    useVxeTable(app);
  },
};
