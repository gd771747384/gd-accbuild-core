/**
 * 包括: 1. 远程运行时元数据配置;2. 本地代码中的元数据配置;3. 画布设计时配置。
 */
 export default {
    /* 远程运行时元数据配置 开始 */
    templateIdx: {
        type: Number,
        default: -1,
    },
    activedVmCodeInGroup: {
        type: [String, Object],
        default: () => null,
    },
    isLoading: {
        type: Boolean,
        default: false,
    },
    /* 远程运行时元数据配置 结束 */
    /* 本地代码中的元数据配置 开始*/
    mainKey: {
        type: [String, Number],
        default: () => "",
    },
    //树弹窗表单元数据
    staticFilterAllBtnsEmitFormItemMetaConfig: {
        type: Object,
        default: () => ({}),
    },
    //树弹窗表单动态元数据
    staticFilterDialogFormItemDynmicConfigList: {
        type: Array,
        default: () => ([]),
    },
    //树元数据不需要
    //树数据
    staticFilterTableBodyData: {
        type: Array,
        default: () => ([]),
    },
    //主表元数据
    staticTableColMetaList: {
        type: Array,
        default: () => ([]),
    },
    //主表动态元数据
    staticTableColumnDynmicConfigList: {
        type: Array,
        default: () => ([]),
    },
    //主表弹窗表单元数据
    staticAllBtnsEmitFormItemMetaConfig: {
        type: Object,
        default: () => ({}),
    },
    //主表弹窗表单动态元数据
    staticDialogFormItemDynmicConfigList: {
        type: Array,
        default: () => ([]),
    },
    //主表数据
    staticTableBodyData: {
        type: Array,
        default: () => ([]),
    },
    //主表搜索表单元数据
    staticSearchFormItemMetaList: {
        type: Array,
        default: () => ([]),
    },
    //主表搜索表单动态元数据
    staticSearchFormItemDynmicConfigList: {
        type: Array,
        default: () => ([]),
    },
    //主表搜索表单数据
    staticSearchFormData: {
        type: Array,
        default: () => ([]),
    },
    /////////////////
    staticResource: {
        type: Object,
        default: () => ({}),
        /* a: {
            filter_tableCommonAttr: {
                toolBarConfig: {
                    size: "default",
                    buttons: [{
                        key: "buildInAddBtn"
                    }],
                },
                operationBtnsConfig: {
                    size: "default",
                    buttons: [{
                        key: "buildInEditBtn",
                    },
                    {
                        key: "buildInAddChildIntoCurBtn",
                    },
                    {
                        key: "buildInRowDeleteBtn",
                    }],
                },
                strategys:{

                }
            },
            tableCommonAttr: {
                toolBarConfig: {
                    size: "default",
                    buttons: [
                        {
                            key: "buildInAddBtn",
                        },
                        {
                            key: "buildInBatchDeleteBtn",
                        },
                    ],
                },
                operationBtnsConfig: {
                    size: "default",
                    buttons: [
                        {
                            key: "buildInAddChildIntoCurBtn",
                        },
                        {
                            key: "buildInRowDeleteBtn",
                        },
                        {
                            key: "buildInEditBtn",
                        },
                        {
                            key: "buildInDetailBtn",
                        },
                    ],
                },
                strategys:{
                    
                }
            }
        } */
    },
    staticMetaConfigs:{
        type:Array,
        default:()=>([])
    },
    /* 本地代码中的元数据配置 结束*/
    /* 画布设计时配置 */
    canvasRenderConfig: {
        type: Object,
        default: () => ({}),
    },
}