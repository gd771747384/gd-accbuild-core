import { globalConfig } from "@gd-accbuild-core/config";
export default ({ _store }) => {
  fetch(
    `${location.origin}${
      globalConfig["BASE_STATIC_PREFIX_PATH"]
    }/static/components/allowRemoteLoadComps.json?random=${
      Math.floor(Math.random() * (1 - 1000)) + 1000
    }`
  )
    .then(async (res) => {
      const content = await res.text();
      let v = [];
      if (content) {
        v = JSON.parse(content);
      }
      _store.dispatch("settings/setAllowRemoteLoadComps", v);
    })
    .catch((err) => {
      _store.dispatch("settings/setAllowRemoteLoadComps", []);
    });
  globalConfig.getUserResource().then((res) => {
    _store.dispatch("permission/generateRoutes", res).then(
      (generateRes) => {},
      (error) => {
        console.log(error);
      }
    );
  });
};
