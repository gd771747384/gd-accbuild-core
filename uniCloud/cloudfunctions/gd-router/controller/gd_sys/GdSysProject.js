const {
	Controller
} = require("uni-cloud-router");
let GdSysProjectController = class x extends Controller {};
try {
  const {
    GdSysProject_GdSysProjectController,
  } = require("gd-accbuild-designer-utils");
  GdSysProjectController = GdSysProject_GdSysProjectController;
} catch (e) {
  //console.log(e, "报错")
}
module.exports = GdSysProjectController;
