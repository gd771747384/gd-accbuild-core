const {
	Controller
} = require("uni-cloud-router");
module.exports = class CommonController extends Controller {
	generateCode() {
		return this.service.gd_sys.Common.generateCode(this.ctx.event);
	}
	//自用
	resetDatabaseTableId() {
		return this.service.gd_sys.Common.resetDatabaseTableId(this.ctx.event);
	}
};