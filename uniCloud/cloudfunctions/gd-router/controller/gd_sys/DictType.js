const {
	Controller
} = require("uni-cloud-router");
module.exports = class DictTypeController extends Controller {
	getListByVmCode() {
		return this.service.gd_sys.DictType.getListByVmCode(this.ctx.event);
	}
	// getByVmCode() {
	// 	return this.service.gd_sys.DictType.getListByVmCode(this.ctx.event);
	// }
};
