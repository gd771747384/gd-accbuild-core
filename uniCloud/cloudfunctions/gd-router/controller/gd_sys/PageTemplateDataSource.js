const { Controller } = require("uni-cloud-router");
module.exports = class PageTemplateDataSourceController extends Controller {
  async getPassiveReferenceEntityList() {
    return await this.service.gd_sys.PageTemplateDataSource.getPassiveReferenceEntityList(this.ctx.event);
  }
  async checkAutoNamedIsExist(){
	return await this.service.gd_sys.PageTemplateDataSource.checkAutoNamedIsExist(this.ctx.event);  
  }
  async createViewModelAndDefaultViewProps(){
	  return await this.service.gd_sys.PageTemplateDataSource.createViewModelAndDefaultViewProps(this.ctx.event);
  }
};