const {
	Service
} = require("uni-cloud-router");
let GdSysProjectService = class x extends Service {};
try {
  const {
    GdSysProject_GdSysProjectService,
  } = require("gd-accbuild-designer-utils");
  GdSysProjectService = GdSysProject_GdSysProjectService;
} catch (e) {
  //console.log(e, "报错")
}
module.exports = GdSysProjectService;
