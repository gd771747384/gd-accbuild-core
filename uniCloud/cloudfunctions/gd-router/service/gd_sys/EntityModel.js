const {
	Service
} = require("uni-cloud-router");
const jqlHandler = require('gd-accbuild-jql-handler')
const generateId = require('generate-id')
module.exports = class EntityModelService extends Service {
	async getListByVmCode({
		strategyName,
		params,
		data,
		vmCode: crudVmCode,
		dbJql
	}) {
		console.log("debug1:", params)
		if (params.parentId === null) {
			const res = await dbJql.collection('gd_sys_own_module').get();
			console.log("debug2:", res)
			return {
				errCode: res.code,
				errMsg: res.message,
				data: {
					data: {},
					items: res.data.map(el => ({
						"id": el.module_code,
						"parentId": null,
						"entityCode": "",
						"dbCode": null,
						"displayName": el.display_name,
						"ordinal": el.ordinal,
						"hasChildren": true
					}))
				}
			}
		} else {
			const res = await dbJql.collection('gd_sys_entity_model').where({
				"parent_id": params.parentId
			}).get();
			return {
				errCode: res.code,
				errMsg: res.message,
				data: {
					data: {},
					items: res.data.map(el => ({
						"id": el.id,
						"parentId": el.parent_id,
						"entityCode": el.entity_code,
						"dbCode": el.db_code,
						"displayName": el.display_name,
						"ordinal": el.ordinal,
						"commonInheritField": el.common_inherit_field,
						"hasChildren": false
					}))
				}
			}
		}
	}
	async initAutoIncreaseTableData({
		strategyName,
		params,
		data,
		vmCode: crudVmCode,
		dbJql
	}) {
		let ret = {
			errCode: -1,
			errMsg: `模块${data.moduleCode}表${data.entityCode},自增辅助创建成功`,
			data: {
				data: {},
				items: []
			}
		}
		if (data.moduleCode && data.entityCode) {
			const insertId = await generateId()
			const res = await dbJql.collection('gd_sys_auto_increase').add({
				id: insertId,
				module_code: data.moduleCode,
				entity_code: data.entityCode,
				num: 0
			})
			if (res.id) {
				ret.errCode = 0;
				ret.errMsg = `模块${data.moduleCode}表${data.entityCode},自增辅助创建失败`
			}
		}
		return ret
	}
};
