import { globalConfig } from "@gd-accbuild-core/config";
import { getUrlPath } from "@gd-accbuild-core/config/utils";
export const getIsShowLayout = () => {
	/* javascript-obfuscator:disable */
  const curPageInfoStr = uni.getStorageSync("store/permission/curPageInfo");
  /* javascript-obfuscator:enable */
  let curPageInfo = {};
  if (curPageInfoStr) {
    curPageInfo = JSON.parse(curPageInfoStr);
  }
  console.log(curPageInfo,"curPageInfo=======")
  const appendExistsLayoutUrls = [];
  const curLayout = curPageInfo?.resource?.uiOptions?.layoutStyle ?? "";
  if (curLayout !== "none") {
    appendExistsLayoutUrls.push(curPageInfo.targetResourceUrl);
  }
  const curAddressPath = getUrlPath();
  console.log(curAddressPath,'地址==')
  const isHiddenLayout = globalConfig.pagesLayout.some(
    (page) =>
      page.layoutStyle === "none" &&
      [`/${page.path}`, ...page.alias, ...appendExistsLayoutUrls].includes(
        curAddressPath
      )
  );
  const isShow = !isHiddenLayout;
  return isShow;
};
