import { onUnload, onShow, onHide } from "@dcloudio/uni-app";
import { useRouter } from "vue-router"; //因为uniapp h5使用的是vue-router
import { getCompleteUrl, globalConfig } from "@gd-accbuild-core/config";
import { getUrlPath } from "@gd-accbuild-core/config/utils";
/**
 * 监听页面切换设置curPageInfo
 */
export const useResetCurPageInfoOnToggleWindowTab = (store) => {
  const $router = useRouter();
  const routes = $router.getRoutes();
  onShow(() => {
    const curAddressPath = getUrlPath();
    const matchRoute = routes.find((route) => curAddressPath === route.path);
    if (matchRoute && Array.isArray(store.state.permission.pages)) {
      const curPageInfo = store.state.permission.pages.find(
        (page) =>
          getCompleteUrl({ realPageKey: page.realPageKey }) ===
          matchRoute.meta.route
      );
      store.commit("permission/SET_CURPAGEINFO", curPageInfo);
    }
  });
  /* onHide(() => {
        console.log("页面隐藏");
    }); */
};
//https://segmentfault.com/a/1190000022822185
const listenUrlChange = ({ callback = () => {} }) => {
  window.addEventListener("beforeunload", callback);
  if (globalConfig.ROUTER_MODE === "hash") {
    window.addEventListener("hashchange", () => {
      store.commit(
        "historyRoute/SET_ISHASHCHANGE",
        !store.state.historyRoute.isHashChange
      );
      //beforeunloadFn();
      callback();
    });
  } else {
    window.addEventListener("pushState", function (e) {
      store.commit(
        "historyRoute/SET_ISHASHCHANGE",
        !store.state.historyRoute.isHashChange
      );
      callback();
    });
    /* window.addEventListener('replaceState', function(e) {
      console.log('change replaceState');
    }); */
  }
  window.addEventListener("popstate", function (event) {
    callback();
  });
};

const clearListenUrlChange = ({ callback = () => {} }) => {
  window.removeEventListener("beforeunload", callback);
  if (globalConfig.ROUTER_MODE === "hash") {
    window.removeEventListener("hashchange", callback);
  } else if (globalConfig.ROUTER_MODE === "history") {
    window.removeEventListener("pushState", callback);
  }
  window.removeEventListener("popstate", callback);
};
/**
 * 监听页面刷新按钮,把保存当前页面信息,用于onLaunch时跳转使用
 */
export const useHackFixedPcAdminRefreshOnUnload = (store) => {
  const beforeunloadFn = (e) => {
    const targetResourceUrl = getUrlPath();

    let matchedPageInfo = store.state.permission.pages.find(
      (page) => page.targetResourceUrl === targetResourceUrl
    );

    //刷新、关闭时调用
    if (
      store.state.permission.curPageInfo &&
      targetResourceUrl === store.state.permission.curPageInfo.targetResourceUrl
    ) {
      matchedPageInfo = store.state.permission.curPageInfo;
    } else if (!matchedPageInfo) {
      //TODO:FIXME:
      matchedPageInfo = {
        targetResourceUrl: "/pages/gd-accbuild-development-mgr/home/index",
        path: "/pages/gd-accbuild-development-mgr/home/index",
        realPageKey: "home",
        title: "首页",
      };
    }
    store.commit("permission/SET_CURPAGEINFO", matchedPageInfo);
  };
  listenUrlChange({ callback: beforeunloadFn });
  onUnload(() => {
    clearListenUrlChange({ callback: beforeunloadFn });
  });
};
