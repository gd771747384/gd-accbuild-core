import {
  isSuccessReqBackend,
  getBackendData,
} from "@gd-accbuild-core/config/utils";
import { globalConfig } from "@gd-accbuild-core/config";
import pcadmin_globalStyles from "@gd-accbuild-core/theme-chalk/pc-admin/js.module.scss";
const DefaultCurPageContainerStyle = {
  "pc-admin": {
    height: `100vh - ${pcadmin_globalStyles.accbuildPageTopWindowContainerHeight}`,
  },
  mobile: {},
};
const DefaultBizContainerStyle = {
  "pc-admin": {
    height: `calc(100vh - ${pcadmin_globalStyles.accbuildPageTagsContainerHeight} - ${pcadmin_globalStyles.accbuildPageTopWindowContainerHeight})`,
    padding: pcadmin_globalStyles.accbuildDefaultContainerGutter,
  },
  mobile: {},
};
const state = {
  /* javascript-obfuscator:disable */
  curLocalization: uni.getStorageSync("curLocalization") || "zh-Hans",
  applicationConfiguration:
    JSON.parse(uni.getStorageSync("applicationConfiguration") || "{}") || null,
  allowRemoteLoadComps: JSON.parse(
    uni.getStorageSync("settings/allowRemoteLoadComps") || "[]"
  ),
  /* javascript-obfuscator:enable */
  //TODO:FIXME: 暂不需要
  //页面容器的样式
  curPageContainerStyle:
    DefaultCurPageContainerStyle[globalConfig.targetPlatform],
  //biz容器的样式
  bizContainerStyle: DefaultBizContainerStyle[globalConfig.targetPlatform],
};

const mutations = {
  SET_CURLOCALIZATION: (state, data) => {
    /* javascript-obfuscator:disable */
    uni.setStorageSync("curLocalization", data);
    /* javascript-obfuscator:enable */
    state["curLocalization"] = data;
  },
  SET_APPLICATIONCONFIGURATION: (state, data) => {
    /* javascript-obfuscator:disable */
    uni.setStorageSync("applicationConfiguration", JSON.stringify(data));
    /* javascript-obfuscator:enable */
    state["applicationConfiguration"] = data;
  },
  SET_ALLOWREMOTELOADCOMPS: (state, data) => {
    /* javascript-obfuscator:disable */
    uni.setStorageSync("settings/allowRemoteLoadComps", JSON.stringify(data));
    /* javascript-obfuscator:enable */
    state["allowRemoteLoadComps"] = data;
  },

  SET_CURPAGECONTAINERSTYLE: (state, data) => {
    state["curPageContainerStyle"] =
      data ?? DefaultCurPageContainerStyle[globalConfig.targetPlatform];
  },
};

const actions = {
  setCurLocationzation: async ({ commit }, data) => {
    commit("SET_CURLOCALIZATION", data);
  },
  setApplicationConfiguration: async ({ commit }, curLocalization) => {
    commit("SET_CURLOCALIZATION", curLocalization);
    const res = await globalConfig.getApplicationConfiguration();
    if (isSuccessReqBackend(res)) {
      commit("SET_APPLICATIONCONFIGURATION", getBackendData(res));
    }
  },
  setCurPageContainerStyle: async ({ commit }, data) => {
    commit("SET_CURPAGECONTAINERSTYLE", data);
  },
  setAllowRemoteLoadComps: async ({ commit }, data) => {
    commit("SET_ALLOWREMOTELOADCOMPS", data);
  },
  resetCurPageContainerStyle: async ({ commit }, data) => {
    commit("SET_CURPAGECONTAINERSTYLE", data);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
