import { login, wxLogin, logout } from '@/services/common'

export default {
    namespaced: true,
    state: {
		/* javascript-obfuscator:disable */
        userInfo: uni.getStorageSync('userInfo') || {},
        tokenInfo: uni.getStorageSync('token'),
        token: uni.getStorageSync('token') || '123',
		/* javascript-obfuscator:enable */
    },
    mutations: {
        setUserInfo(state, userInfo) {
            state.userInfo = userInfo
			/* javascript-obfuscator:disable */
            uni.setStorageSync('userInfo', userInfo)
			/* javascript-obfuscator:enable */
        },
        setToken(state, token) {
            state.token = token
			/* javascript-obfuscator:disable */
            uni.setStorageSync('token', token)
			/* javascript-obfuscator:enable */
        }
    },
    actions: {
        login({
            commit
        }, data) {
            return login(data).then(res => {
                commit('setToken', res.token)
                commit('setUserInfo', res.userInfo)
            })
        },
        wxLogin({
            commit
        }, data) {
            return wxLogin(data).then(res => {
                commit('setToken', res.token)
                commit('setUserInfo', res.userInfo)
            })
        },
        logout({
            commit
        }) {
            // return http.post('auth/user/logout').then(res => {
            // 	commit('setToken', '')
            // 	commit('setUserInfo', {})
            // })
            return new Promise(resolve => {
                commit('setToken', '')
                commit('setUserInfo', {})
                resolve()
            })
        }
    }
}
