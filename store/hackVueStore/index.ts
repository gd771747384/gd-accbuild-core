//是对vuex的二次封装，以便后续可以切换底层store，vuex、pinia。
// #if--def STORE_LIB_TYPE-PINIA
//import { defineStore as pinia_defineStore } from "pinia";
// #end--if
// #if--def STORE_LIB_TYPE-VUEX
import { createStore, useStore as vuex_useStore } from "vuex";
// #end--if
interface HackModules {
  [propname: string]: any;
}
type GetModuleName = (modulePath: string) => string;
interface GetCreateStoreParams {
  modulesFiles: any;
  getModuleNameCallback: GetModuleName;
}
let modules: HackModules = {};
export const getCreateStore = (params: GetCreateStoreParams) => {
  const modulesFiles = params["modulesFiles"];
  modules = Object.keys(modulesFiles).reduce((modules, modulePath) => {
    // set './app.js' => 'app'
    const moduleName: string = params["getModuleNameCallback"](modulePath);
    const value = modulesFiles[modulePath] as any;
    modules[moduleName] = value;
    return modules;
  }, {});
  return createStore({
    modules,
  });
};

type HackUseStore = () => {
  state: any;
  getters: any;
  commit: any;
  dispatch: any;
};
let _useStore: HackUseStore;
// #if--def STORE_LIB_TYPE-PINIA
// const _pinia_useStore: HackUseStore = () => ({
//   state: {},
//   getters: {},
//   commit: {},
//   dispatch: {},
// });
// const pinia_useStore = new Proxy(_pinia_useStore, {
//   apply: function (target, ctx, args) {
//     //console.log("拦截前", target, Object.keys(ctx), args);
//     //TODO:FIXME: 拦截里面的 state、getters、commit、dispatch
//     // modules
//     return pinia_defineStore("GlobalStore", () => ({
//       state: {},
//       getters: {},
//       commit: {},
//       dispatch: {},
//     }));
//   },
// });
// _useStore = pinia_useStore; // 使用 proxy拦截方法调用
// #end--if
// #if--def STORE_LIB_TYPE-VUEX
_useStore = vuex_useStore;
// #end--if
//useXXX可以在这里面调用;前提是useStore必须在页面顶层调用,不能运行时调用
export const useStore = (isMockStore = false) => {
  if (!isMockStore) {
    return _useStore();
  } else {
    return {};
  }
};

//TODO:FIXME: pinia兼容计划，使用vuex的方式调用pinia
/* 
{
  state,
  getters,
  commit,
  dispatch
}
使用 proxy拦截属性访问与修改、拦截useStore方法调用
1. 改造state
如访问store.state.permission.a: 
 拦截state的属性的getter 获取 对应模块名为permission，调用动态函数名 store["usePermissionStore"]()，返回 对应的state对象的 值
2. 改造dispatch
如访问store.dispatch("permission/getA",params): 
 在dispatch方法内判断第一个参数，获取 对应模块名为permission，调用动态函数名 store["usePermissionStore"](),返回 对应的action 的 值
3. 改造commit、getters同上
 */
// 定义store注意点：为保证兼容vuex4.0,mutations第一个参数为state、actions第一个参数为当前 use···Store() == context == {commit}
